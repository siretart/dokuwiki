# dokuwiki po-debconf translation to Spanish
# Copyright (C) 2005, 2008, 2009, 2011 Software in the Public Interest
# This file is distributed under the same license as the dokuwiki package.
#
# Changes:
#   - Initial translation
#       César Gómez Martín <cesar.gomez@gmail.com>, 2005
#
#   - Updates
#       Francisco Javier Cuadrado <fcocuadrado@gmail.com>, 2008, 2009, 2011
#       Matías Bellone <matiasbellone+debian@gmail.com>, 2013
#
# Traductores, si no conocen el formato PO, merece la pena leer la
# documentación de gettext, especialmente las secciones dedicadas a este
# formato, por ejemplo ejecutando:
#       info -n '(gettext)PO Files'
#       info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor, lean antes de traducir
# los siguientes documentos:
#
#   - El proyecto de traducción de Debian al español
#     http://www.debian.org/intl/spanish/
#     especialmente las notas de traducción en
#     http://www.debian.org/intl/spanish/notas
#
#   - La guía de traducción de po's de debconf:
#     /usr/share/doc/po-debconf/README-trans
#     o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
msgid ""
msgstr ""
"Project-Id-Version: dokuwiki 0.0.20101107a-2\n"
"Report-Msgid-Bugs-To: dokuwiki@packages.debian.org\n"
"POT-Creation-Date: 2013-10-27 19:00+0100\n"
"PO-Revision-Date: 2013-12-20 19:54-0300\n"
"Last-Translator: Matías Bellone <matiasbellone+debian@gmail.com>\n"
"Language-Team: Debian l10n Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "apache2"
msgstr "apache2"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "lighttpd"
msgstr "lighttpd"

#. Type: multiselect
#. Description
#: ../templates:1002
msgid "Web server(s) to configure automatically:"
msgstr "Servidor/es web a configurar automáticamente:"

#. Type: multiselect
#. Description
#: ../templates:1002
msgid ""
"DokuWiki runs on any web server supporting PHP, but only listed web servers "
"can be configured automatically."
msgstr ""
"Puede ejecutar DokuWiki sobre cualquier servidor web que permita utilizar "
"PHP, pero sólo se pueden configurar automáticamente los servidores web "
"enumerados."

#. Type: multiselect
#. Description
#: ../templates:1002
msgid ""
"Please select the web server(s) that should be configured automatically for "
"DokuWiki."
msgstr ""
"Escoja el/los servidor/es web que se deberían configurar automáticamente "
"para DokuWiki."

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Should the web server(s) be restarted now?"
msgstr "¿Desea reiniciar el/los servidor/es web ahora?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"In order to activate the new configuration, the reconfigured web server(s) "
"have to be restarted."
msgstr ""
"Para poder activar la nueva configuración, se tiene/n que reiniciar el/los "
"servidor/es web reconfigurado/s."

#. Type: string
#. Description
#: ../templates:3001
msgid "Wiki location:"
msgstr "Ubicación del wiki:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Specify the directory below the server's document root from which DokuWiki "
"should be accessible."
msgstr ""
"Indique el directorio bajo la raíz del servidor web donde DokuWiki debería "
"estar accesible."

#. Type: select
#. Description
#: ../templates:4001
msgid "Authorized network:"
msgstr "Red autorizada:"

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"Wikis normally provide open access to their content, allowing anyone to "
"modify it. Alternatively, access can be restricted by IP address."
msgstr ""
"Normalmente, los wikis proporcionan acceso abierto a su contenido, "
"permitiendo a cualquiera editarlo. Sin embargo, puede restringir el "
"acceso por dirección IP."

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"If you select \"localhost only\", only people on the local host (the machine "
"the wiki is running on) will be able to connect. \"local network\" will "
"allow people on machines in a local network (which you will need to specify) "
"to talk to the wiki. \"global\" will allow anyone, anywhere, to connect to "
"the wiki."
msgstr ""
"Si selecciona «sólo la máquina local», sólo se podrán conectar quienes lo "
"hagan desde la máquina local (la máquina en la que se está ejecutando el "
"wiki). Si selecciona «red local» se permitirá la conexión con el wiki a "
"quienes lo hagan desde la red local (la cual debe especificar). Si selecciona "
"«global» se permitirá la conexión con el wiki a cualquier persona desde cualquier "
"lugar."

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"The default is for site security, but more permissive settings should be "
"safe unless you have a particular need for privacy."
msgstr ""
"El valor predeterminado es el más seguro, pero una configuración más "
"permisiva debería ser igualmente segura a menos que tenga una necesidad "
"concreta de privacidad."

#. Type: select
#. Choices
#: ../templates:4002
msgid "localhost only"
msgstr "sólo la máquina local"

#. Type: select
#. Choices
#: ../templates:4002
msgid "local network"
msgstr "red local"

#. Type: select
#. Choices
#: ../templates:4002
msgid "global"
msgstr "global"

#. Type: string
#. Description
#: ../templates:5001
msgid "Local network:"
msgstr "Red local:"

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"The specification of your local network should either be an IP network in "
"CIDR format (x.x.x.x/y) or a domain specification (like .example.com)."
msgstr ""
"Puede definir su red local mediante una dirección IP en formato CIDR "
"(x.x.x.x/y) o usando un dominio (como «.example.com»)."

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"Anyone who matches this specification will be given full and complete access "
"to DokuWiki's content."
msgstr ""
"Cualquier equipo con estos parámtros tendrá acceso total y completo al "
"contenido de DokuWiki."

#. Type: boolean
#. Description
#: ../templates:6001
msgid "Purge pages on package removal?"
msgstr "¿Desea purgar las páginas al eliminar el paquete?"

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"By default, DokuWiki stores all its pages in a file database in /var/lib/"
"dokuwiki."
msgstr ""
"De manera predeterminada, DokuWiki almacena todas sus páginas en una base "
"de datos en el archivo «/var/lib/dokuwiki»."

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"Accepting this option will leave you with a tidier system when the DokuWiki "
"package is removed, but may cause information loss if you have an "
"operational wiki that gets removed."
msgstr ""
"Si acepta esta opción su sistema quedará más limpio cuando elimine el paquete "
"DokuWiki, pero puede que pierda información si se elimina un wiki operativo."

#. Type: boolean
#. Description
#: ../templates:7001
msgid "Make the configuration web-writeable?"
msgstr ""
"¿Desea otorgar permisos de escritura sobre la configuración al servidor web?"

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"DokuWiki includes a web-based configuration interface. To be usable, it "
"requires the web server to have write permission to the configuration "
"directory."
msgstr ""
"DokuWiki incluye una interfaz de configuración basada en la web. Para que se "
"pueda utilizar, necesita que el servidor web tenga permisos de escritura "
"sobre el directorio de configuración."

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"Accepting this option will give the web server write permissions on the "
"configuration directory and files."
msgstr ""
"Al aceptar esta opción dará permisos de escritura al servidor web sobre el "
"directorio y los archivos de configuración."

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"The configuration files will still be readable and editable by hand "
"regardless of whether or not you accept this option."
msgstr ""
"Podrá leer y editar los archivos de configuración a mano, independientemente "
"de si aceptó o no esta opción."

#. Type: boolean
#. Description
#: ../templates:8001
msgid "Make the plugins directory web-writeable?"
msgstr ""
"¿Desea permitir la escritura en el directorio de complementos desde la web?"

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"DokuWiki includes a web-based plugin installation interface. To be usable, "
"it requires the web server to have write permission to the plugins directory."
msgstr ""
"DokuWiki incluye una interfaz de instalación de complementos basada en la "
"web. Para que se pueda utilizar, necesita que el servidor web tenga permisos "
"de escritura sobre el directorio de complementos."

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Accepting this option will give the web server write permissions to the "
"plugins directory."
msgstr ""
"Al aceptar esta opción dará permisos de escritura al servidor web sobre el "
"directorio de complementos."

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Plugins can still be installed by hand regardless of whether or not you "
"accept this option."
msgstr ""
"Podrá instalar complementos a mano, independientemente de si acepta o no "
"esta opción."

#. Type: string
#. Description
#: ../templates:9001
msgid "Wiki title:"
msgstr "Título del wiki:"

#. Type: string
#. Description
#: ../templates:9001
msgid ""
"The wiki title will be displayed in the upper right corner of the default "
"template and on the browser window title."
msgstr ""
"El título del wiki se mostrará en la esquina superior derecha de la "
"plantilla predeterminada y en el título de la ventana del navegador web."

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC0 \"No Rights Reserved\""
msgstr "CC0 «Sin derechos de autor»"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution"
msgstr "CC Reconocimiento"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-ShareAlike"
msgstr "CC Reconocimiento-CompartirIgual"

#. Type: select
#. Choices
#: ../templates:10001
msgid "GNU Free Documentation Licence"
msgstr "Licencia de documentación libre de GNU"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-NonCommercial"
msgstr "CC Reconocimiento-NoComercial"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-NonCommercial-ShareAlike"
msgstr "CC Reconocimiento-NoComercial-CompartirIgual"

#. Type: select
#. Description
#: ../templates:10002
msgid "Wiki license:"
msgstr "Licencia del wiki:"

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"Please choose the license you want to apply to your wiki content. If none of "
"these licenses suits your needs, you will be able to add your own to the "
"file /etc/dokuwiki/license.php and to refer it in the main configuration "
"file /etc/dokuwiki/local.php when the installation is finished."
msgstr ""
"Seleccione la licencia que desea aplicar al contenido de su wiki. Si ninguna "
"de estas licencias se ajusta a sus necesidades, podrá agregar la que desea "
"en el archivo «/etc/dokuwiki/license.php» y hacer referencia a ella en el "
"archivo de configuración principal «/etc/dokuwiki/local.php» cuando finalice "
"la instalación."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"Creative Commons \"No Rights Reserved\" is designed to waive as many rights "
"as legally possible."
msgstr ""
"La licencia Creative Commons «Sin derechos de autor» se diseñó para renunciar "
"a todos los derechos posibles legalmente."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution is a permissive license that only requires licensees to give "
"credit to the author."
msgstr ""
"CC Reconocimiento es una licencia permisiva que sólo requiere que los "
"licenciatarios reconozcan el crédito del autor."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution-ShareAlike and GNU Free Documentation License are copyleft-"
"based free licenses (requiring modifications to be released under similar "
"terms)."
msgstr ""
"CC Reconocimiento-CompartirIgual y la Licencia de documentación libre de GNU "
"son licencias basadas en el «copyleft» (requieren que las modificaciones "
"sean publicadas en términos similares)."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution-NonCommercial and CC Attribution-Noncommercial-ShareAlike are "
"non-free licenses, in that they forbid commercial use."
msgstr ""
"CC Reconocimiento-NoComercial y Reconocimiento-NoComercial-CompartirIgual no "
"son licencias libres, ya que prohíben el uso comercial."

#. Type: boolean
#. Description
#: ../templates:11001
msgid "Enable ACL?"
msgstr "¿Desea activar la lista de control de acceso (ACL)?"

#. Type: boolean
#. Description
#: ../templates:11001
msgid ""
"Enable this to use an Access Control List for restricting what the users of "
"your wiki may do."
msgstr ""
"Si activa esta opción podrá utilizar una lista de control de acceso para "
"restringir qué pueden hacer los usuarios del wiki."

#. Type: boolean
#. Description
#: ../templates:11001
msgid ""
"This is a recommended setting because without ACL support you will not have "
"access to the administration features of DokuWiki."
msgstr ""
"Se recomienda activar esta opción porque sin ella no tendrá acceso a las "
"funciones de administración de DokuWiki."

#. Type: string
#. Description
#: ../templates:12001
msgid "Administrator username:"
msgstr "Nombre de usuario del administrador:"

#. Type: string
#. Description
#: ../templates:12001
msgid ""
"Please enter a name for the administrator account, which will be able to "
"manage DokuWiki's configuration and create new wiki users. The username "
"should be composed of lowercase ASCII letters only."
msgstr ""
"Introduzca un nombre para la cuenta del administrador, la cual podrá "
"gestionar la configuración de DokuWiki y crear nuevos usuarios del wiki. El "
"nombre de usuario debería tener sólo letras ASCII minúsculas."

#. Type: string
#. Description
#: ../templates:12001
msgid ""
"If this field is left blank, no administrator account will be created now."
msgstr ""
"Si deja este campo en blanco, no se creará ahora ninguna cuenta de "
"administrador."

#. Type: string
#. Description
#: ../templates:13001
msgid "Administrator real name:"
msgstr "Nombre real del administrador:"

#. Type: string
#. Description
#: ../templates:13001
msgid ""
"Please enter the full name associated with the wiki administrator account. "
"This name will be stored in the wiki password file as an informative field, "
"and will be displayed with the wiki page changes made by the administrator "
"account."
msgstr ""
"Introduzca el nombre completo asociado a la cuenta del administrador del "
"wiki. Este nombre se almacenará en el archivo de contraseñas del wiki como "
"un campo informativo y se mostrará en los cambios de las páginas del wiki "
"realizados por la cuenta del administrador."

#. Type: string
#. Description
#: ../templates:14001
msgid "Administrator email address:"
msgstr "Dirección de correo electrónico del administrador:"

#. Type: string
#. Description
#: ../templates:14001
msgid ""
"Please enter the email address associated with the wiki administrator "
"account. This address will be stored in the wiki password file, and may be "
"used to get a new administrator password if you lose the original."
msgstr ""
"Introduzca la dirección de correo electrónico asociada a la cuenta del "
"administrador del wiki. Esta dirección se almacenará en el archivo de "
"contraseñas del wiki y se utilizará para obtener una contraseña nueva para "
"el administrador si éste la pierde."

#. Type: password
#. Description
#: ../templates:15001
msgid "Administrator password:"
msgstr "Contraseña del administrador:"

#. Type: password
#. Description
#: ../templates:15001
msgid "Please choose a password for the wiki administrator."
msgstr "Escoja una contraseña para el administrador del wiki."

#. Type: password
#. Description
#: ../templates:16001
msgid "Re-enter password to verify:"
msgstr "Vuelva a introducir la contraseña para comprobarla:"

#. Type: password
#. Description
#: ../templates:16001
msgid ""
"Please enter the same \"admin\" password again to verify you have typed it "
"correctly."
msgstr ""
"Introduzca la misma contraseña de «administración» para comprobar que la ha "
"escrito correctamente."

#. Type: error
#. Description
#: ../templates:17001
msgid "Password input error"
msgstr "Se ha producido un error al introducir la contraseña"

#. Type: error
#. Description
#: ../templates:17001
msgid "The two passwords you entered were not the same. Please try again."
msgstr ""
"Las dos contraseñas que ha introducido no son iguales. Inténtelo nuevamente."

#. Type: select
#. Description
#: ../templates:18001
msgid "Initial ACL policy:"
msgstr "Política inicial de la ACL:"

#. Type: select
#. Description
#: ../templates:18001
msgid ""
"Please select what initial ACL configuration should be set up to match the "
"intended usage of this wiki:\n"
" \"open\":   both readable and writeable for anonymous users;\n"
" \"public\": readable for anonymous users, writeable for registered users;\n"
" \"closed\": readable and writeable for registered users only."
msgstr ""
"Seleccione qué configuración inicial debería tener la ACL dependiendo del "
"uso que le vaya a dar a este wiki:\n"
" «abierta»: permisos de lectura y de escritura para los usuarios anónimos.\n"
" «pública»: permisos de lectura para los usuarios anónimos y de escritura "
"para los registrados.\n"
" «cerrada»: permisos de lectura y escritura sólo para usuarios registrados."

#. Type: select
#. Description
#: ../templates:18001
msgid ""
"This is only an initial setup; you will be able to adjust the ACL rules "
"later."
msgstr ""
"Esto sólo es una configuración inicial, podrá ajustar las reglas de la ACL "
"más tarde."

#. Type: select
#. Choices
#: ../templates:18002
msgid "open"
msgstr "abierta"

#. Type: select
#. Choices
#: ../templates:18002
msgid "public"
msgstr "pública"

#. Type: select
#. Choices
#: ../templates:18002
msgid "closed"
msgstr "cerrada"

#~ msgid "Please choose the license you want to apply to your wiki content."
#~ msgstr "Escoja la licencia que quiere aplicar al contenido del wiki."

#~ msgid ""
#~ "DokuWiki will be accessible through a directory of your website. By "
#~ "default, this is http://yourserver/dokuwiki, but you can change it to be "
#~ "anything within your server.  Enter just the directory portion below."
#~ msgstr ""
#~ "Se podrá acceder a DokuWiki mediante el directorio de su página web. De "
#~ "manera predeterminada, éste es «http://su-servidor/docuwiki», pero puede "
#~ "cambiarlo a cualquier cosa en su servidor. Introduzca el directorio aquí "
#~ "debajo."

#~ msgid ""
#~ "A Wiki is normally used to provide unlimited access to information, which "
#~ "can be freely modified by anyone.  Since this is not always wanted,  it "
#~ "is possible to restrict access to the site on the basis of the "
#~ "originating IP address."
#~ msgstr ""
#~ "Un wiki se usa normalmente para proporcionar acceso ilimitado a "
#~ "información, que cualquiera puede modificar libremente. Puesto que a "
#~ "veces esto no es lo que se quiere, es posible restringir el acceso al "
#~ "sitio web en base a la dirección IP de origen."

#~ msgid ""
#~ "For security, this is set to 'localhost only' by default.  Unless you "
#~ "have a particular need for privacy on your Wiki, you should be able to "
#~ "allow access globally without compromising site security."
#~ msgstr ""
#~ "De forma predeterminada, se configura como «sólo en local» por seguridad. "
#~ "A menos que tenga una necesidad particular de intimidad en su wiki "
#~ "debería ser capaz de permitir el acceso global sin comprometer la "
#~ "seguridad del sitio web."

#~ msgid "Web servers:"
#~ msgstr "Servidores web:"

#~ msgid ""
#~ "DokuWiki can be used with any of the given web servers.  Select the "
#~ "servers which you would like DokuWiki to be installed into."
#~ msgstr ""
#~ "DokuWiki se puede usar con cualquiera de los servidores web "
#~ "proporcionados. Seleccione los servidores en los que le gustaría instalar "
#~ "DokuWiki."

#~ msgid "apache"
#~ msgstr "apache"

#~ msgid "apache-ssl"
#~ msgstr "apache-ssl"

#~ msgid "apache-perl"
#~ msgstr "apache-perl"

#~ msgid "Where should the web-accessible location of the DokuWiki be?"
#~ msgstr "¿Donde debería estar instalado DokuWiki para ser accesible vía web?"

#~ msgid "Who should be able to access your DokuWiki?"
#~ msgstr "¿Quién debe ser capaz de acceder a su Dokuwiki?"

#~ msgid "localhost only, local network, global"
#~ msgstr "sólo en local, red local, global"

#~ msgid "/dokuwiki"
#~ msgstr "/dokuwiki"

#~ msgid "10.0.0.0/24"
#~ msgstr "10.0.0.0/24"

#~ msgid "apache, apache2, apache-ssl, apache-perl"
#~ msgstr "apache, apache2, apache-ssl, apache-perl"

#~ msgid "Which web servers do you want to install into?"
#~ msgstr "¿En qué servidores le gustaría instalar Dokuwiki?"

#~ msgid ""
#~ "DokuWiki can install into any of the given web servers.  All those you "
#~ "specify will have a default config line added to them to install a "
#~ "standard DokuWiki with the defaults you specified earlier."
#~ msgstr ""
#~ "Se puede instalar Dokuwiki en cualquiera de los servidores web "
#~ "especificados. A todos los que especifique se le añadirá una línea de "
#~ "configuración por omisión para instalar Dokuwiki estándar con las "
#~ "opciones por omisión que haya especificado."
